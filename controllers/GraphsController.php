<?php
namespace app\controllers;
use Yii;
use yii\web\Session;
use app\models\Ahrefs;
use app\models\Backlinks;

class GraphsController extends \yii\web\Controller
{

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionReport($url = null){

		$backlinks = new Backlinks;
		$data = $backlinks->getMetrics($url);
		if ($data['error']['status'] == true){
			Yii::$app->session->setFlash('error', $data['error']['message']);
			$this->redirect('/graphs');
		}
		return 	$this->render(
					'charts', [
						'metrics' => $data['metrics']->metrics,
						'percentage' => $data['percentage'],
						'percentage_nofollow_dofollow' => $data['percentage_nofollow_dofollow']
					]
				);
	}

	public function actionNofollow($url, $type, $count = false){
		$backlinks = new Backlinks;
		if ($url && $type && !$count)
			$backlinks->prepearDataLostNewBacklinksAjax($backlinks->getNofollow($url, $type));

		if ($url && $type && $count)
			$backlinks->prepearDataTotalLostNewBacklinksAjax($backlinks->getNofollow($url, $type));
	}

}
