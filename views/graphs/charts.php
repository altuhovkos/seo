<?php
use yii\helpers\Html;
?>



<h2 align='center' class='title'>Результат отчета по сайту - <b><?= Yii::$app->request->get('url'); ?></b></h2>
<div data-url='<?= Yii::$app->request->get('url'); ?>' id='url' style='display:none;'></div>
<div class="container">
  <div class="row">
    <div class="center border">

      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <h4 align='center'>Total Active Backlinks</h4>
        <tbody>
            <tr>
              <td>Total Active Backlinks</td>
              <td><?= Html::encode($metrics->backlinks) ?></td>
            </tr>
            <tr>
              <td>IMG links</td>
              <td><?= Html::encode($metrics->image) ?></td>
            </tr>
            <tr>
              <td>Nofollow Links</td>
              <td><?= Html::encode($metrics->nofollow) ?></td>
            </tr>

            <tr>
              <td>Links to Home Page </td>
              <td><?= Html::encode($metrics->pages) ?></td>
            </tr>
            <tr>
              <td>Links to Other Page </td>
              <td><?= Html::encode($metrics->refpages) ?></td>
            </tr>

        </tbody>
    </table>
    </div>

    <div class="center border">
      <h4 align='center'>Text and links pictures</h4>
      <div id="links_pictures"></div>
      <p>
      	<span><b>Links Image:</b> <?= Html::encode($metrics->image) ?> (<?= Html::encode($percentage['percentage_pictures']) ?> %)</span><br>
      	<span><b>Text anchors:</b> <?= Html::encode($metrics->text) ?> (<?= Html::encode($percentage['percentage_text']) ?> %)</span>
      </p>
      <p>
    		<?php if ($percentage['percentage_pictures'] <= 8 ): ?>
    			<b>Вывод: </b>На ваш сайт ссылается не достаточное количество ссылок картинок, ваш ссылочный профиль выглядит не естественно.
    		<?php elseif ($percentage['percentage_pictures'] > 8 ): ?>
    			<b>Вывод: </b>Ссылочная масса вашего сайта содержит достаточно ссылок-картинок. Это придает ей естественный вид.
    		<?php endif; ?>
      </p>
    </div>

    <div class="center border">
      <h4 align='center'>Nofollow and dofollow links</h4>
      <div id="links_bofollow_dofollow"></div>
      <p>
        <span><b>Nofollow:</b> <?= Html::encode($metrics->nofollow) ?> (<?= Html::encode($percentage_nofollow_dofollow['nofollow']) ?> %)</span><br>
        <span><b>Dofollow:</b> <?= Html::encode($metrics->dofollow) ?> (<?= Html::encode($percentage_nofollow_dofollow['dofollow']) ?> %)</span>
      </p>
      <p>
        <?php if ($percentage_nofollow_dofollow['nofollow'] <= 15 ): ?>
          <b>Решение: </b>Необходимо увеличить количество ссылающих доменов с nofollow атрибутом.
        <?php elseif ($percentage_nofollow_dofollow['dofollow'] > 15 ): ?>
          <b>Решение: </b>Продолжайте наращивать ноуфолов ссылки на ваш сайт, это обезопасит вас от попадание под фильтр.
        <?php endif; ?>
      </p>
    </div>

  </div>
</div>

<div id="container_nofollow" class='block'></div>
<div id="container_dofollow" class='block'></div>
<div id="total_num" class='block'></div>
<script id="links_pictures_json">
     [
      {"label": "Links Image", "value": <?= Html::encode($percentage['percentage_pictures']) ?>},
      {"label": "Text anchors", "value": <?= Html::encode($percentage['percentage_text']) ?> }
    ]
</script>
<script id="links_bofollow_dofollow_json">
     [
      {"label": "nofollow", "value": <?= Html::encode($percentage_nofollow_dofollow['nofollow']) ?>},
      {"label": "dofollow", "value": <?= Html::encode($percentage_nofollow_dofollow['dofollow']) ?> }
    ]
</script>
