$(document).ready(function() {
    var url = $('#url').data('url');
    if (url) {
        nofollow(url);
        dofollow(url);
        total(url)
    }
});

function nofollow(url){
    $.ajax({
        url: '/graphs/nofollow',
        type: 'GET',
        dataType: 'json',
        data: {url: url, type: 'true'},
        beforeSend : function(xhr, opts){
            $('#container_nofollow').addClass('preload');
        },
        success: function(recponse){
            $('#container_nofollow').removeClass('preload');
            noffolowTotalActiveBacklinks(recponse.data.date,
                                        recponse.data.new_total,
                                        recponse.data.lost_total,
                                        recponse.min,
                                        recponse.max,
                                        '#container_nofollow',
                                        'Nofollow Total Active Backlinks'
                                        );
        }
    });
}

function dofollow(url){
    $.ajax({
        url: '/graphs/nofollow',
        type: 'GET',
        dataType: 'json',
        data: {url: url, type: 'false'},
        beforeSend : function(xhr, opts){
            $('#container_dofollow').addClass('preload');
        },
        success: function(recponse){
            $('#dontainer_nofollow').removeClass('preload');
            noffolowTotalActiveBacklinks(recponse.data.date,
                                         recponse.data.new_total,
                                         recponse.data.lost_total,
                                         recponse.min,
                                         recponse.max,
                                         '#container_dofollow',
                                         'Dofollow Total Active Backlinks'
                                         );
        }
    });
}

function total(url){
    $.ajax({
        url: '/graphs/nofollow',
        type: 'GET',
        dataType: 'json',
        data: {url: url, type: 'false', count: true},
        beforeSend : function(xhr, opts){
            $('#total_num').addClass('preload');
        },
        success: function(recponse){
            $('#total_num').removeClass('preload');
            totalChart(recponse.date, recponse.total);
        }
    });
}

function noffolowTotalActiveBacklinks(categories, new_total, lost_total, min, max, element_id, description){

    $(element_id).highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: description
        },
        xAxis: [{
            categories: categories,
            reversed: false,
            labels: {
                step: 1
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: categories,
            linkedTo: 0,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return (Math.abs(this.value));
                }
            },
            min: min,
            max: max
        },

        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + ', Date ' + this.point.category + '</b><br/>' +
                    'Total backlinks: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
            }
        },

        series: [{
            name: 'Lost backlinks',
            data: lost_total
        }, {
            name: 'New backlinks',
            data: new_total
        }]
    });


}

function totalChart(dates, results){
  $('#total_num').highcharts({
      title: {
          text: 'Stock market index',
          x: -20 //center
      },
      subtitle: {
          text: '',
          x: -20
      },
      xAxis: {
          categories: dates
      },
      yAxis: {
          title: {
              text: 'counts'
          },
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
      },
      tooltip: {
          valueSuffix: ' links'
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
      },
      series: [{
          name: 'Stock Merket',
          data: results
      }]
  });
}
$(document).ready(function() {
    Morris.Donut({
          element: 'links_pictures',
          data: $.parseJSON($('#links_pictures_json').html())
        });
        Morris.Donut({
        element: 'links_bofollow_dofollow',
        data: $.parseJSON($('#links_bofollow_dofollow_json').html())
    });

});