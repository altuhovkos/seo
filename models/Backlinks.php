<?php

namespace app\models;
use Yii;
use app\models\Ahrefs;

class Backlinks extends \yii\base\Object{

	public $access_token = '3c1bf611b4c9e98812a04b67b988d285e1f2bb60';

	public $refresh_token = '46f80b91a10a0adcaa94e7498a38fc8e137eb2ef';

	public $api;

	function __construct() {
		$this->api = new Ahrefs($this->access_token, false);
	}

	public function getMetrics($url, $limit = 100, $offset = 0){

		if ($url){
			if (!$this->cacheChecker($url)) {
				$this->api->set_target($url)->mode_subdomains()->set_offset($offset)->set_limit($limit)->set_output('json');
				$metrics = json_decode($this->api->get_metrics_extended());

				if (isset($metrics->error) || $metrics->metrics->backlinks < 1)
					return ['error' => ['status' => true, 'message' => 'Данных не получено']];

				$percentage['percentage_pictures'] = ceil($metrics->metrics->image / $metrics->metrics->text * 100);
				$percentage['percentage_text'] = ceil(100 - $percentage['percentage_pictures']);
				$percentage_nofollow_dofollow['nofollow'] = ceil($metrics->metrics->nofollow / $metrics->metrics->dofollow * 100);
				$percentage_nofollow_dofollow['dofollow'] = ceil(100 - $percentage_nofollow_dofollow['nofollow']);
				$result = array(
								'metrics' => $metrics,
								'percentage' => $percentage,
								'percentage_nofollow_dofollow' => $percentage_nofollow_dofollow,
								'error' => array('status' => false),
								);

				Yii::$app->cache->set($url, $result, 86400);
			}else
				$result = Yii::$app->cache->get($url);

			return $result;
		}else
			return ['error' => ['status' => true, 'message' => 'Пустой URL']];

	}

	public function getNofollow($url, $type, $limit = 100){

		if ($type == 'false')
			$check = 'dofollow';
		else
			$check = 'nofollow';

		if (!$this->cacheChecker($url, $check)) {
			$this->api->set_target($url)->mode_subdomains()->set_limit($limit)->set_output('json');
			$this->api->where_eq('nofollow', $type);
			$result = $this->api->get_backlinks_new_lost_counters();
			$this->cacheSeter($url, $check, $result);
		}else
			$result = $this->cacheGetter($url, $check);

		return $result;

	}

	public function getRedirect($url, $limit = 100){
		$this->api->set_target('$url')->mode_subdomains()->set_limit(100)->set_output('json');
		$this->api->where_ne('redirect','0');
		$result = $this->api->get_backlinks_new_lost_counters();
		return $result;
	}


	public function cacheSeter($url, $key, $data) {
	    Yii::$app->cache->set($url . $key, $data, 86400);
	}

	public function cacheGetter($url, $key = '') {
	   return Yii::$app->cache->get($url . $key);
	}

	public function cacheChecker($url, $key = ''){
		return !empty(Yii::$app->cache->get($url . $key));
	}


	public function prepearDataLostNewBacklinksAjax($json){
		$result = json_decode($json);

		$counters = [['date'], ['new_total'], ['lost_total']];

		foreach ($result->counts as $value) {
			$counters['date'][] = $value->date;
			$counters['new_total'][] = $value->new_total;
			$counters['lost_total'][] = -1 * $value->lost_total;
		}

		echo json_encode(array('data' => $counters, 'min' => min($counters['lost_total']), 'max' => max($counters['new_total'])));
		Yii::$app->end();
	}

	public function prepearDataTotalLostNewBacklinksAjax($json){
		$result = json_decode($json);
		$counters = [['total'], ['date']];

		foreach ($result->counts as $value){
			$counters['total'][] = $value->new_total + $value->lost_total;
			$counters['date'][] = $value->date;
		}

		echo json_encode($counters);
		Yii::$app->end();
	}

}